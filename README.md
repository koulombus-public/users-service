#Spring-Boot Users Application with mongoDB
The goals of this project are:

- A user service with JWT functionality.
- 
- Test using JUnit5.

## Application

- ### users-service

  `Spring Boot` Web Java application to manage users. The data is stored in `MySQL`

  ![user-service-swagger](documentation/user-service-swagger.png)

## Application profile

- local ```-Dspring.profiles.active=local```
- docker ```-Dspring.profiles.active=docker```

## Prerequisites

- [`Java 17`](https://www.oracle.com/java/technologies/downloads/#java11)
- [`Docker`](https://www.docker.com/)
- [`Docker-Compose`](https://docs.docker.com/compose/install/)

## Start Environment

- Open a terminal and inside `user-service` root folder run
  ```
  docker-compose up -d
  ```

- Wait for `MySQL` Docker container to be up and running. To check it, run
  ```
  docker-compose ps
  ```

> The file docker-compose.yml hold also the configuration for postgres. This service and pgadmin are commented out.

## Start Application

- In a terminal, make sure you are in `user-service` root folder

- Build the Project
  ```
  ./mvn package
  ```
  
- Run application
  ```
  ./mvn user-service:clean user-service:bootRun
  ```












- Swagger website is http://localhost:8080/swagger-ui.html

## Shutdown

- Go to the terminal where `user-service` is running and press `Ctrl+C`

- In a terminal and inside `springboot-testing-mysql` root folder, run to command below to stop and remove docker-compose `mysql` container and network
  ```
  docker-compose down -v
  ```

## Running Unit and Integration Testing

- In a terminal, navigate to `springboot-testing-mysql` root folder

- Run unit and integration tests
  ```
  ./gradlew user-service:clean user-service:assemble \
    user-service:cleanTest \
    user-service:test \
    user-service:integrationTest
  ```
  > **Note:** During the tests, `Testcontainers` starts automatically `MySQL` Docker container before the tests begin and shuts it down when the tests finish.

- **Unit Testing Report** can be found at
  ```
  user-service/build/reports/tests/test/index.html
  ```

- **Integration Testing Report** can be found at
  ```
  user-service/build/reports/tests/integrationTest/index.html
  ```














#JWT Application

A sample application how to implement a JWT functionality.

Check the video to this session [here](https://www.youtube.com/watch?v=VVn9OG9nfH0).

All ***Postman*** request are located under `/postman`

#Spring boot security
With Spring boot security dependency a standard login will display when an api will be called.

After a successful login there is a possibility to check the access_token.
Copy your access_token from the request `/login` and check it [here](jwt.io)

####Decoded
HEADER:ALGORITHM & TOKEN TYPE
```
{
  "typ": "JWT",
  "alg": "HS256"
}
```
PAYLOAD:DATA
```
{
  "sub": "John1",
  "roles": "ROLE_MANAGERROLE_USER",
  "iss": "http://localhost:8090/login",
  "exp": 1647953321
}
```
