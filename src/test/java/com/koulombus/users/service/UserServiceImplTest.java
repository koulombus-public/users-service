package com.koulombus.users.service;

import static com.koulombus.users.security.ScGrantedAuthority.ADMIN;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

import java.security.Principal;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import com.koulombus.users.domain.RoleModel;
import com.koulombus.users.domain.UserModel;
import com.koulombus.users.repository.UsersRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;

@ExtendWith(MockitoExtension.class)
class UserServiceImplTest {

    @Mock
    private UsersRepository userRepository;

    @Mock
    private PasswordEncoder passwordEncoder;

    @InjectMocks
    private UsersServiceImpl underTest;

    @Test
    void canSaveNewUser() {
        // given
        String username = "username";
        UserModel userModel = new UserModel(UUID.randomUUID().toString(), //
                                            username + "_1 " + username + "_2", //
                                            username, //
                                            "password", //
                                            List.of(new RoleModel(ADMIN)));
        // when
        underTest.registerNewUser(userModel);

        // then
        // Argument capture will capture the userModel
        // --> return userRepository.save(userModel);
        ArgumentCaptor<UserModel> userModelArgumentCaptor = ArgumentCaptor.forClass(UserModel.class);
        // The result of the capture will save in userModelArgumentCaptor
        verify(userRepository).save(userModelArgumentCaptor.capture());
        UserModel capturedUserModel = userModelArgumentCaptor.getValue();
        assertThat(capturedUserModel).isEqualTo(userModel);
    }

    @Test
    void canUpdateAnExistingUser() {
        // given
        String username = "username";
        UserModel userModel = new UserModel(UUID.randomUUID().toString(), //
                                            username + "_1 " + username + "_2", //
                                            username, //
                                            "password", //
                                            List.of(new RoleModel(ADMIN)));

        // ... return an optional of roleModel
        given(userRepository.findById(userModel.getId())).willReturn(Optional.of(userModel));

        // when
        underTest.updateUser(userModel);

        // then
        ArgumentCaptor<UserModel> userModelArgumentCaptor = ArgumentCaptor.forClass(UserModel.class);
        verify(userRepository).save(userModelArgumentCaptor.capture());
        UserModel capturedUserModel = userModelArgumentCaptor.getValue();
        assertThat(capturedUserModel).isEqualTo(userModel);
    }

    @Test
    void itShouldFindAllUsers() {
        // when
        underTest.getUsers();

        // then
        verify(userRepository).findAll();
    }

    @Test
    void itShouldFindUserByName() {
        // given
        String username = "username";
        UserModel userModel = new UserModel(UUID.randomUUID().toString(), //
                                            username + "_1 " + username + "_2", //
                                            username, //
                                            "password", //
                                            List.of(new RoleModel(ADMIN)));

        // ... return optional of userModel
        given(userRepository.findByUsername(username)).willReturn(Optional.of(userModel));

        // when
        underTest.getUserWithName(username);

        // then
        verify(userRepository).findByUsername(username);
    }

    @Test
    void willThrowUserNotFoundExceptionWhenTryToFindNotExistingUserByName() {
        Assertions.assertThrows(UsernameNotFoundException.class, () ->
                underTest.getUserWithName("username")
        );
    }

    @Test
    void itShouldFindUserWithId() {
        // given
        String username = "username";
        String id = UUID.randomUUID().toString();
        UserModel userModel = new UserModel(id, //
                                            username + "_1 " + username + "_2", //
                                            username, //
                                            "password", //
                                            List.of(new RoleModel(ADMIN)));

        // ... return optional of userModel
        given(userRepository.findById(id)).willReturn(Optional.of(userModel));

        // when
        underTest.getUserWithId(id);

        // then
        verify(userRepository).findById(id);
    }

    @Test
    void itShouldGetFullUser() {
        // given
        String username = "principalUsername";
        Principal principal = new Principal() {
            @Override
            public String getName() {
                return username;
            }
        };
        UserModel userModel = new UserModel(UUID.randomUUID().toString(), //
                                            username + "_1 " + username + "_2", //
                                            username, //
                                            "password", //
                                            List.of(new RoleModel(ADMIN)));

        // ... return optional of userModel
        given(userRepository.findByUsername(username)).willReturn(Optional.of(userModel));

        // when
        underTest.getFullUser(principal);

        // then
        verify(userRepository).findByUsername(username);
    }

    @Test
    void itShouldDeleteUserWithId() {
        // given
        String id = UUID.randomUUID().toString();

        // when
        underTest.deleteUserWithId(id);

        // then
        verify(userRepository).deleteById(id);
    }

    @Test
    @Disabled
    void itShouldLoadUserByUsername() {
        // given
        String username = "username";
        UserModel userModel = new UserModel(UUID.randomUUID().toString(), //
                                            username + "_1 " + username + "_2", //
                                            username, //
                                            "password", //
                                            List.of(new RoleModel(ADMIN)));

        // ... return optional of userModel
        given(userRepository.findByUsername(username)).willReturn(Optional.of(userModel));

        // when
        // then
    }
}
