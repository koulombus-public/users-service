package com.koulombus.users.service;

import static com.koulombus.users.security.ScGrantedAuthority.ADMIN;
import static com.koulombus.users.security.ScGrantedAuthority.USER;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.verify;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import com.koulombus.users.domain.RoleModel;
import com.koulombus.users.domain.UserModel;
import com.koulombus.users.repository.RoleRepository;
import com.koulombus.users.repository.UsersRepository;
import com.koulombus.users.security.ScGrantedAuthority;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class RoleServiceImplTest {

    @Mock
    private RoleRepository roleRepository;

    @Mock
    private UsersRepository userRepository;

    @InjectMocks
    private RoleServiceImpl underTest;

    /**
     * ArgumentCaptor allows us to capture an argument passed to a method in order to inspect it.
     * This is especially useful when we can't access the argument outside the method we'd like to test.
     */
    @Captor
    private ArgumentCaptor<RoleModel> roleModelArgumentCaptor;

    @Captor
    private ArgumentCaptor<UserModel> userModelArgumentCaptor;

    @Test
    void itShouldSaveNewRole() {
        // given
        RoleModel roleModel = new RoleModel(ADMIN);

        // no role with roleName passed.
        given(roleRepository.findByName(ADMIN)).willReturn(Optional.empty());

        // when
        underTest.saveRole(roleModel);

        // then
        then(roleRepository).should().save(roleModelArgumentCaptor.capture());
        RoleModel roleModelArgumentCaptorValue = roleModelArgumentCaptor.getValue();
        assertThat(roleModelArgumentCaptorValue).usingRecursiveComparison() //
                                                .ignoringFields("id") //
                                                .isEqualTo(roleModel);
    }

    @Test
    @Disabled
    void itShouldAddRoleUserToUserWithExistingRoleAdmin() {
        // given
        String username = "username";
        RoleModel roleModel = new RoleModel(USER);
        UserModel userModel = new UserModel(UUID.randomUUID().toString(), //
                                            username + "_1 " + username + "_2", //
                                            username, //
                                            "password", //
                                            List.of(new RoleModel(ADMIN)));

        // role with roleName passed.
//        given(roleRepository.findByName(USER)).willReturn(Optional.of(roleModel));
//        given(userRepository.findById(userModel.getId())).willReturn(Optional.of(userModel));

        // when
        underTest.addRoleToUser(userModel.getId(), USER);

        // then
        ArgumentCaptor<ScGrantedAuthority> roleModelArgumentCaptor = ArgumentCaptor.forClass(ScGrantedAuthority.class);
        verify(roleRepository).findByName(roleModelArgumentCaptor.capture());
        UserModel capturedUserModel = userModelArgumentCaptor.getValue();
        assertThat(capturedUserModel).isEqualTo(userModel);

        ArgumentCaptor<String> userIdArgumentCaptor = ArgumentCaptor.forClass(String.class);
        verify(userRepository).findById(userIdArgumentCaptor.capture());
        String capturedUserId = userIdArgumentCaptor.getValue();
        assertThat(capturedUserId).isEqualTo(userModel.getId());

        ArgumentCaptor<UserModel> userModelArgumentCaptor = ArgumentCaptor.forClass(UserModel.class);
        verify(userRepository).save(userModelArgumentCaptor.capture());
        UserModel capturedUserModel2 = userModelArgumentCaptor.getValue();
        assertThat(capturedUserModel2).isEqualTo(userModel);
    }

    @Test
    void itShouldGetRoles() {
        // given
        RoleModel roleModel = new RoleModel(ADMIN);

        // return list of roleModel
        given(roleRepository.findAll()).willReturn(List.of(roleModel));

        // when
        RoleModel foundRoleModel = underTest.getRoles().get(0);

        // then
        assertThat(roleModel).usingRecursiveComparison() //
                             .ignoringFields("id") //
                             .isEqualTo(foundRoleModel);
    }

    @Test
    void itShouldDeleteRole() {
        // given
        RoleModel roleModel = new RoleModel(ADMIN);

        // when
        underTest.deleteRole(roleModel.getName());

        // then
        verify(roleRepository).deleteByName(roleModel.getName());
    }
}
