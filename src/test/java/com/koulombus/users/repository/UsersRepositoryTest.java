package com.koulombus.users.repository;

import static com.koulombus.users.security.ScGrantedAuthority.ADMIN;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

import java.util.List;
import java.util.Optional;

import com.koulombus.users.domain.RoleModel;
import com.koulombus.users.domain.UserModel;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.test.context.TestPropertySource;

/**
 * Spring Boot 2.6 Release Notes
 * <p>
 * Embedded Mongo
 * To use embedded mongo, the spring.mongodb.embedded.version property must now be set.
 * This helps to ensure that the MongoDB version that is used by the embedded support
 * matches the MongoDB version that your application will use in production.
 */
@TestPropertySource(properties = "spring.mongodb.embedded.version=3.5.5")
@DataMongoTest
class UsersRepositoryTest {

    @Autowired
    private UsersRepository underTest;

    @Test
    void itShouldFindUserByUsername() {
        // given
        UserModel userModel = new UserModel(null, //
                                            "TestA1 TestB1", //
                                            "TestC1", //
                                            "1234", //
                                            List.of(new RoleModel(ADMIN)));
        underTest.save(userModel);

        // when
        Optional<UserModel> optionalUserModel = underTest.findByUsername(userModel.getUsername());

        // then
        assertThat(optionalUserModel).isPresent() //
                                     .hasValueSatisfying(u -> {
                                         assertThat(u).usingRecursiveComparison() //
                                                      .ignoringFields("id") //
                                                      .isEqualTo(userModel);
                                     });
    }

    @Test
    void itShouldNotFindAnyUssrByUsernameWhenTheUserNotExist() {
        // given
        String userNotExist = "userNotExist";

        // when
        Optional<UserModel> userModel = underTest.findByUsername(userNotExist);

        // then
        assertThat(userModel).isEmpty();
    }
}
