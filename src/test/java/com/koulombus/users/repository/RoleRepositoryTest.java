package com.koulombus.users.repository;

import static com.koulombus.users.security.ScGrantedAuthority.ADMIN;
import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;

import java.util.Optional;

import com.koulombus.users.domain.RoleModel;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.test.context.TestPropertySource;

/**
 * Spring Boot 2.6 Release Notes
 * <p>
 * Embedded Mongo
 * To use embedded mongo, the spring.mongodb.embedded.version property must now be set.
 * This helps to ensure that the MongoDB version that is used by the embedded support
 * matches the MongoDB version that your application will use in production.
 */
@TestPropertySource(properties = "spring.mongodb.embedded.version=3.5.5")
@DataMongoTest
class RoleRepositoryTest {

    @Autowired
    private RoleRepository underTest;

    @Test
    void itShouldFindByName() {
        // given
        RoleModel roleModel = new RoleModel(ADMIN);
        underTest.save(roleModel);

        // when
        Optional<RoleModel> optionalRoleModel = underTest.findByName(roleModel.getName());

        // then
        assertThat(optionalRoleModel) //
                                      .isPresent() //
                                      .hasValueSatisfying(r -> {
                                          assertThat(r).usingRecursiveComparison() //
                                                       .ignoringFields("id") //
                                                       .isEqualTo(roleModel);
                                      });
    }
}
