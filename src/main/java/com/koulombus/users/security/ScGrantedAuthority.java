package com.koulombus.users.security;

public enum ScGrantedAuthority {
    USER,
    ADMIN,
    MANAGER
}
