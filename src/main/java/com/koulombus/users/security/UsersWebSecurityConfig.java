package com.koulombus.users.security;

import static org.springframework.security.config.http.SessionCreationPolicy.STATELESS;

import com.koulombus.users.filter.CustomAuthenticationFilter;
import com.koulombus.users.filter.CustomAuthorizationFilter;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
@EnableWebSecurity
@RequiredArgsConstructor
public class UsersWebSecurityConfig extends WebSecurityConfigurerAdapter {

    private final UserDetailsService userDetailsService;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override
    protected void configure(AuthenticationManagerBuilder auth)
            throws Exception {
        auth.userDetailsService(userDetailsService).passwordEncoder(bCryptPasswordEncoder);
    }

    @Override
    protected void configure(HttpSecurity http)
            throws Exception {

        // Change default /login url
        CustomAuthenticationFilter customAuthenticationFilter = new CustomAuthenticationFilter(authenticationManagerBean());
        customAuthenticationFilter.setFilterProcessesUrl("/api/v1/login");

        // Cross Site Request Forgery
        http.csrf().disable();

        http.sessionManagement().sessionCreationPolicy(STATELESS);
        http.authorizeHttpRequests() //

            // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            // !!! The order is very important !!!
            // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

            // Login
            .antMatchers("/api/v1/login/**", "/api/v1/token/refresh/**").permitAll() //

            // User management is only available to Admins.
            // User
            .antMatchers("/api/v1/user/**").hasAnyAuthority(ScGrantedAuthority.ADMIN.name()) //

            // Role
            .antMatchers("/api/v1/role/**").hasAnyAuthority(ScGrantedAuthority.ADMIN.name()) //

            // Any user can get their own details.
            .antMatchers(HttpMethod.GET, "/api/v1/user/full/**") //
            .authenticated() //
        ;

        // #1 Authentication filter
        http.addFilter(customAuthenticationFilter);
        // #2 Authorization filter
        http.addFilterBefore(new CustomAuthorizationFilter(), UsernamePasswordAuthenticationFilter.class);
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean()
            throws Exception {
        return super.authenticationManagerBean();
    }
}
