package com.koulombus.users.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

public class JsonUtil {

    private static final ObjectMapper MAPPER;

    static {
        MAPPER = new ObjectMapper();
        MAPPER.findAndRegisterModules();
        MAPPER.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
        MAPPER.disable(DeserializationFeature.ADJUST_DATES_TO_CONTEXT_TIME_ZONE);
        MAPPER.enable(SerializationFeature.INDENT_OUTPUT);
    }

    /**
     * Convert object to an easily readable JSON string with indentation.
     * <p>
     * NOTE: Normally this should only be used for debugging.
     * </P>
     *
     * @param obj
     *         Object to convert.
     * @return The given object converted to an easily readable JSON string with indentation.
     */
    public static String toStringPrettyPrint(Object obj) {
        String jsonInString;
        try {
            jsonInString = MAPPER.writeValueAsString(obj);
        } catch (JsonProcessingException e) {
            return "{\"conversion-error\":\"" + e + "\"}";
        }
        return jsonInString;
    }

}
