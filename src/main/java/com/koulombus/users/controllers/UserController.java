package com.koulombus.users.controllers;

import java.security.Principal;
import java.util.List;

import com.koulombus.users.domain.UserModel;
import com.koulombus.users.service.UsersService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1")
@AllArgsConstructor
public class UserController {

    private final UsersService userService;

    @GetMapping("/user/all")
    public ResponseEntity<List<UserModel>> getUsers() {
        return new ResponseEntity<>(userService.getUsers(), HttpStatus.OK);
    }

    @GetMapping("/user/{userId}")
    public ResponseEntity<UserModel> getUserWithId(@PathVariable String userId) {
        return new ResponseEntity<>(userService.getUserWithId(userId), HttpStatus.OK);
    }

    @GetMapping("/user/full")
    public ResponseEntity<UserModel> getFullUser(Principal principal) {
        return new ResponseEntity<>(userService.getFullUser(principal), HttpStatus.OK);
    }

    @PostMapping("/user")
    public ResponseEntity<UserModel> registerUser(@RequestBody UserModel userModel) {
        return new ResponseEntity<>(userService.registerNewUser(userModel), HttpStatus.OK);
    }

    @PutMapping("/user")
    public ResponseEntity<UserModel> updateUser(@RequestBody UserModel userModel) {
        return new ResponseEntity<>(userService.updateUser(userModel), HttpStatus.OK);
    }

    @DeleteMapping("/user/{userId}")
    public void deleteUser(@PathVariable String userId) {
        userService.deleteUserWithId(userId);
    }
}
