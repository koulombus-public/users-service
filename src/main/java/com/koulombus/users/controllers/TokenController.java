package com.koulombus.users.controllers;

import java.io.IOException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.koulombus.users.service.TokenService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/token/refresh")
@AllArgsConstructor
public class TokenController {

    private final TokenService tokenService;

    @GetMapping
    public void refreshToken(HttpServletRequest request, HttpServletResponse response)
            throws IOException {
        tokenService.refreshToken(request, response);
    }
}
