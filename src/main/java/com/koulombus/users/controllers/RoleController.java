package com.koulombus.users.controllers;

import java.util.List;

import com.koulombus.users.domain.RoleModel;
import com.koulombus.users.domain.RoleToUserModel;
import com.koulombus.users.security.ScGrantedAuthority;
import com.koulombus.users.service.RoleServiceImpl;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/role")
@AllArgsConstructor
public class RoleController {

    private final RoleServiceImpl roleService;

    @PostMapping
    public ResponseEntity<RoleModel> saveRole(@RequestBody RoleModel authority) {
        return new ResponseEntity<>(roleService.saveRole(authority),HttpStatus.OK);
    }

    @PostMapping("/addToUser")
    public ResponseEntity<?> addRoleToUser(@RequestBody RoleToUserModel roleToUserModel) {
        roleService.addRoleToUser(roleToUserModel.getUserId(), roleToUserModel.getRoleName());
        return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }

    @GetMapping("/all")
    public List<RoleModel> getRoles() {
        return roleService.getRoles();
    }

    @DeleteMapping("/{roleName}")
    public void deleteRole(@PathVariable ScGrantedAuthority roleName) {
        roleService.deleteRole(roleName);
    }
}
