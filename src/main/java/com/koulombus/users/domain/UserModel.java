package com.koulombus.users.domain;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document
@AllArgsConstructor
public class UserModel {

    @Id
    private String id = UUID.randomUUID().toString();
    private String fullName;
    private String username;
    private String password;
    private List<RoleModel> authorities = new ArrayList<>();
}
