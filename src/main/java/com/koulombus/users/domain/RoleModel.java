package com.koulombus.users.domain;

import java.util.UUID;

import com.koulombus.users.security.ScGrantedAuthority;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document
public class RoleModel {

    public RoleModel(ScGrantedAuthority name) {
        this.name = name;
    }

    @Id
    private String id = UUID.randomUUID().toString();
    @Indexed(name = "roleName", unique = true)
    private ScGrantedAuthority name;
}
