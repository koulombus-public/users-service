package com.koulombus.users.domain;

import com.koulombus.users.security.ScGrantedAuthority;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class RoleToUserModel {
    private String userId;
    private ScGrantedAuthority roleName;
}
