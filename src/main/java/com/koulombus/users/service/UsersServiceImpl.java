package com.koulombus.users.service;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.koulombus.users.domain.UserModel;
import com.koulombus.users.repository.UsersRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UsersServiceImpl implements UsersService, UserDetailsService {

    private final UsersRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    @Override
    public UserDetails loadUserByUsername(String username)
            throws UsernameNotFoundException {
        Optional<UserModel> userModel = userRepository.findByUsername(username);
        if (userModel.isEmpty()) {
            throw new UsernameNotFoundException("User not found in the database.");
        }

        List<SimpleGrantedAuthority> authorities = new ArrayList<>();
        userModel.get().getAuthorities().forEach(role -> {
            authorities.add(new SimpleGrantedAuthority(role.getName().name()));
        });
        return new User(userModel.get().getUsername(), userModel.get().getPassword(), authorities);
    }

    public UserModel registerNewUser(UserModel userModel) {
        Optional<UserModel> foundUser = userRepository.findByUsername(userModel.getUsername());
        if (foundUser.isEmpty()) {
            userModel.setPassword(passwordEncoder.encode(userModel.getPassword()));
            return userRepository.save(userModel);
        }
        return foundUser.get();
    }

    @Override
    public UserModel updateUser(UserModel userModel) {
        Optional<UserModel> foundUser = userRepository.findById(userModel.getId());
        if (foundUser.isPresent()) {
            foundUser.get().setFullName(userModel.getFullName());
            foundUser.get().setUsername(userModel.getUsername());
            foundUser.get().setPassword(userModel.getPassword());
            foundUser.get().setAuthorities(userModel.getAuthorities());
            return userRepository.save(foundUser.get());
        }
        throw new UsernameNotFoundException(String.format("User with ID [%s] not found for update.", userModel.getId()));
    }

    @Override
    public List<UserModel> getUsers() {
        return userRepository.findAll();
    }

    @Override
    public UserModel getUserWithName(String username) {
        return getUserModel(username);
    }

    @Override
    public UserModel getUserWithId(String userId) {
        return userRepository.findById(userId).orElseThrow(() -> new UsernameNotFoundException(String.format("user with id %s not found.", userId)));
    }

    @Override
    public UserModel getFullUser(Principal principal) {
        return getUserModel(principal.getName());
    }

    @Override
    public void deleteUserWithId(String userId) {
        userRepository.deleteById(userId);
    }

    private UserModel getUserModel(String username) {
        return userRepository.findByUsername(username).orElseThrow(() -> {
            throw new UsernameNotFoundException(String.format("User %s not found in database.", username));
        });
    }

}
