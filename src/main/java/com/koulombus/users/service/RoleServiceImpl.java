package com.koulombus.users.service;

import java.util.List;
import java.util.Optional;

import com.koulombus.users.domain.RoleModel;
import com.koulombus.users.repository.RoleRepository;
import com.koulombus.users.repository.UsersRepository;
import com.koulombus.users.security.ScGrantedAuthority;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class RoleServiceImpl implements RoleService {

    private final RoleRepository roleRepository;
    private final UsersRepository userRepository;

    @Override
    public RoleModel saveRole(RoleModel role) {
        Optional<RoleModel> optionalRoleModel = roleRepository.findByName(role.getName());
        if (optionalRoleModel.isEmpty()) {
            return roleRepository.save(role);
        }
        return optionalRoleModel.get();
    }

    @Override
    public void addRoleToUser(String userId, ScGrantedAuthority roleName) {
        roleRepository.findByName(roleName).ifPresent(model -> {
            userRepository.findById(userId).ifPresent(user -> {
                if (!user.getAuthorities().contains(model)) {
                    user.getAuthorities().add(model);
                    userRepository.save(user);
                }
            });
        });
    }

    @Override
    public List<RoleModel> getRoles() {
        return roleRepository.findAll();
    }

    @Override
    public void deleteRole(ScGrantedAuthority roleName) {
        roleRepository.deleteByName(roleName);
    }
}
