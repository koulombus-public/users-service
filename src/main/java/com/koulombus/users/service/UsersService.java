package com.koulombus.users.service;

import java.security.Principal;
import java.util.List;

import com.koulombus.users.domain.UserModel;

public interface UsersService {

    UserModel registerNewUser(UserModel userModel);

    UserModel updateUser(UserModel userModel);

    List<UserModel> getUsers();

    UserModel getUserWithName(String username);

    UserModel getUserWithId(String userId);

    void deleteUserWithId(String userId);

    UserModel getFullUser(Principal principal);
}
