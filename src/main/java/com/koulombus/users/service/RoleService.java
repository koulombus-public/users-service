package com.koulombus.users.service;

import java.util.List;

import com.koulombus.users.domain.RoleModel;
import com.koulombus.users.security.ScGrantedAuthority;

public interface RoleService {

    RoleModel saveRole(RoleModel authority);

    void addRoleToUser(String userId, ScGrantedAuthority roleName);

    List<RoleModel> getRoles();

    void deleteRole(ScGrantedAuthority roleName);
}
