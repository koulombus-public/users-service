package com.koulombus.users;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    //    // Create default user
    // TODO: move to configuration?
//    @Bean
//    CommandLineRunner run(UsersService userService, RoleService roleService) {
//        return args -> {
//            roleService.saveRole(new RoleModel(ADMIN));
//            roleService.saveRole(new RoleModel(USER));
//            UserModel usermodel = userService.registerNewUser(new UserModel(UUID.randomUUID().toString(), //
//                                                                            "admin admin", //
//                                                                            "admin", //
//                                                                            "1234", //
//                                                                            new ArrayList<>())); roleService.addRoleToUser(usermodel.getId(), ADMIN);
//        };
//    }

}
