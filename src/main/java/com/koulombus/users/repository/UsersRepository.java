package com.koulombus.users.repository;

import java.util.Optional;

import com.koulombus.users.domain.UserModel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UsersRepository extends MongoRepository<UserModel, String> {
    Optional<UserModel> findByUsername(String username);
}

