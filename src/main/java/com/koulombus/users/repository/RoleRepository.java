package com.koulombus.users.repository;

import java.util.Optional;

import com.koulombus.users.domain.RoleModel;
import com.koulombus.users.security.ScGrantedAuthority;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRepository extends MongoRepository<RoleModel, String> {
    Optional<RoleModel> findByName(ScGrantedAuthority name);
    Optional<RoleModel> deleteByName(ScGrantedAuthority name);
}
